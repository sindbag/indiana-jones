using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class S_ButtonControl : MonoBehaviour
{
    public Button button;

    public bool IsActive = false;
    public List <S_ButtonControl> Neighbors;
    public S_LabyrinthManager LabyrinthManager;

    public GameObject Player;

    public int id;

    public void Activate(bool newActive)
    {
        IsActive = newActive;
        button.interactable = newActive;
    }

    public void ActivateNeighbors(bool active)
    {
        foreach (var neighbor in Neighbors)
        {
            neighbor.Activate(active);
        }
    }

    public void MovePlayer()
    {
        foreach (var button in LabyrinthManager.Buttons)
        {
            button.Activate(false);
        }
        Player.transform.position = transform.position;
        Activate(false);
        ActivateNeighbors(true);
    }
}
