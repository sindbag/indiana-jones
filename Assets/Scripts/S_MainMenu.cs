using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class S_MainMenu : MonoBehaviour
{
    public Canvas MainMenu, Rules, Credits, Labirinth, WinScreen;

    Canvas Current;

    private void Start()
    {
        Current = MainMenu;
    }

    void ShowCanvas(Canvas newCanvas)
    {
        Current.enabled = false;
        newCanvas.enabled = true;
        Current = newCanvas;
    }

    public void ShowMainMenu()
    {
        ShowCanvas(MainMenu);
    }
    public void ShowRules()
    {
        ShowCanvas(Rules);
    }

    public void ShowCredits()
    {
        ShowCanvas(Credits);
    }
    public void ShowLabirinth()
    {
        ShowCanvas(Labirinth);
    }
    public void ShowWinScreen()
    {
        ShowCanvas(WinScreen);
    }
}
