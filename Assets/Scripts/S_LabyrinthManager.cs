using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_LabyrinthManager : MonoBehaviour
{
    public List<S_ButtonControl> Buttons;
    int matrixLen;
    void Start()
    {
        matrixLen = (int)Mathf.Sqrt(Buttons.Count);
        for (int i = 0; i < Buttons.Count; i++)
        {
            S_ButtonControl button = Buttons[i];
            button.Activate(false);
            button.id = i;
            button.LabyrinthManager = this;                
        }
        Buttons[Buttons.Count - 1].MovePlayer();
    }
}
